package convert

import (
	"net/http"
	"os"
	"path"
	"strings"
)

// fallbackContentType is the content type return by default
// if there is an error in detection's methods.
const fallbackContentType = "application/octet-stream"

// IsSupportedContentType indicates if the given
// content type is supported by DocTxt Converter.
func IsSupportedContentType(contentType string) bool {
	// TODO to be completed as and when implementations
	// for now, it is planned to accept images and pdf
	return strings.HasPrefix(contentType, "image/") ||
			contentType == "application/pdf"
}

// detectPathContentType try to detect the content type
// of the file whose the path is given. Return `fallbackContentType`
// in case of error.
func detectPathContentType(filepath string) (string, error) {
	contentType := fallbackContentType

	file, err := os.Open(filepath)
	if err != nil {
		return contentType, err
	}
	defer file.Close()

	contentType, err = detectFileContentType(file)
	return contentType, err
}

// detectFileContentType try to detect the content type of the
// given file. Return `fallbackContentType` in case of error.
func detectFileContentType(file *os.File) (string, error) {
	buffer := make([]byte, 512)
	_, err := file.Read(buffer)
	if err != nil {
		return fallbackContentType, err
	}

	// Reset the read pointer if necessary
	file.Seek(0, 0)

	return http.DetectContentType(buffer), nil
}

// detectPathContentTypeByExt determines arbitrary the content
// type of the file, whose the path is given, by it's extension.
func detectPathContentTypeByExt(filepath string) string {
	switch strings.ToLower(path.Ext(filepath)) {
	case ".pdf":
		return "application/pdf"
	case ".doc":
		return "application/msword"
	case ".docx":
		return "application/vnd.openxmlformats-officedocument.wordprocessingml.document"
	case ".odt":
		return "application/vnd.oasis.opendocument.text"
	case ".rtf":
		return "application/rtf"
	case ".xml":
		return "text/xml"
	case ".html", ".htm", ".xhtml":
		return "text/html"
	case ".jpg", ".jpeg", ".jpe", ".jfif", ".jfif-tbnl":
		return "image/jpeg"
	case ".png":
		return "image/png"
	case ".tif":
		return "image/tif"
	case ".tiff":
		return "image/tiff"
	case ".ppm":
		return "image/x-portable-pixmap"
	case ".pnm":
		return "image/x-portable-anymap"
	case ".txt":
		return "text/plain"
	default:
		return fallbackContentType
	}
}
