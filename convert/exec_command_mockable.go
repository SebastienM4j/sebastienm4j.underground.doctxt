package convert

import (
	"os/exec"
)

var command Command = &GoCommand{}

// Command defines methods used with exec.Command().
type Command interface {
	Output(name string, arg ...string) ([]byte, error)
	Run(name string, arg ...string) error
}

// GoCommand implements Command interface with exec.Command
type GoCommand struct {}

// Output runs the command and returns its standard output.
func (g *GoCommand) Output(name string, arg ...string) ([]byte, error) {
	return exec.Command(name, arg...).Output()
}

// Run runs the command.
func (g *GoCommand) Run(name string, arg ...string) error {
	return exec.Command(name, arg...).Run()
}