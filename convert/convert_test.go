package convert

import (
	"testing"
	"io"

	"golang.org/x/net/context"
)

type MockDocumentClient struct {
	ConvertFromPathFunc func(ctx context.Context, path string) (*DocTxt, error)
	ConvertFromReaderFunc func(ctx context.Context, reader io.Reader) (*DocTxt, error)
	CloseFunc func() error
}

func (m *MockDocumentClient) ConvertFromPath(ctx context.Context, path string) (*DocTxt, error) {
	return m.ConvertFromPathFunc(ctx, path)
}

func (m *MockDocumentClient) ConvertFromReader(ctx context.Context, reader io.Reader) (*DocTxt, error) {
	return m.ConvertFromReaderFunc(ctx, reader)
}

func (m *MockDocumentClient) Close() error {
	return m.CloseFunc()
}

func TestConvertPathArgs(t *testing.T) {
	client := &Client{}

	_, err := client.ConvertPath(nil, "")
	if err == nil {
		t.Errorf("[path] arg is not checked for empty value")
	}
}

func TestConvertPathForImageCallImageClient(t *testing.T) {
	client := &Client{}

	imageClientUsed := false
	mockImageClient := MockDocumentClient{
		ConvertFromPathFunc: func(ctx context.Context, path string) (*DocTxt, error) {
			imageClientUsed = true
			return &DocTxt{}, nil
		},
	}
	client.ImageClient = &mockImageClient

	client.ConvertPath(nil, "/tmp/test.jpg")
	
	if !imageClientUsed {
		t.Fatal("The image client have not been used to convert an image")
	}
}

func TestConvertPathForPdfCallPdfClient(t *testing.T) {
	client := &Client{}

	pdfClientUsed := false
	mockPdfClient := MockDocumentClient{
		ConvertFromPathFunc: func(ctx context.Context, path string) (*DocTxt, error) {
			pdfClientUsed = true
			return &DocTxt{}, nil
		},
	}
	client.PdfClient = &mockPdfClient

	client.ConvertPath(nil, "/tmp/test.pdf")
	
	if !pdfClientUsed {
		t.Fatal("The pdf client have not been used to convert a pdf")
	}
}

func TestCloseClientWithAllChildClients(t *testing.T) {
	client := &Client{}

	imageClientClosed := false
	mockImageClient := MockDocumentClient{
		CloseFunc: func() error {
			imageClientClosed = true
			return nil
		},
	}
	client.ImageClient = &mockImageClient

	pdfClientClosed := false
	mockPdfClient := MockDocumentClient{
		CloseFunc: func() error {
			pdfClientClosed = true
			return nil
		},
	}
	client.PdfClient = &mockPdfClient

	client.Close()

	if !imageClientClosed {
		t.Error("The image client have not been closed")
	}
	if !pdfClientClosed {
		t.Error("The pdf client have not been closed")
	}
}

func TestCloseClientWithNoChildClient(t *testing.T) {
	client := &Client{}
	client.Close()
}
