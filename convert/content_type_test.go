package convert

import "testing"

func TestIsSupportedContentTypeMatchImages(t *testing.T) {
	imageContentTypes := []string{
		"application/pdf",
		"image/jpeg",
		"image/png",
		"image/tif",
		"image/tiff",
		"image/x-portable-pixmap",
		"image/x-portable-anymap",
	}
	for _, ct := range imageContentTypes {
		if !IsSupportedContentType(ct) {
			t.Errorf("Content Type [%s] should be supported", ct)
		}
	}
}

func TestIsSupportedContentTypeUnsupported(t *testing.T) {
	unsupportedContentTypes := []string{
		"application/msword",
		"application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"application/vnd.oasis.opendocument.text",
		"application/rtf",
		"text/xml",
		"text/html",
		"text/plain",
	}
	for _, ct := range unsupportedContentTypes {
		if IsSupportedContentType(ct) {
			t.Errorf("Content Type [%s] should not be supported", ct)
		}
	}
}

func TestDetectPathContentTypeByExt(t *testing.T) {
	var pathContentType = map[string]string {
		"/tmp/document.pdf": "application/pdf",
		"/tmp/document.doc": "application/msword",
		"/tmp/document.docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
		"/tmp/document.odt": "application/vnd.oasis.opendocument.text",
		"/tmp/document.rtf": "application/rtf",
		"/tmp/document.xml": "text/xml",
		"/tmp/document.html": "text/html",
		"/tmp/document.htm": "text/html",
		"/tmp/document.xhtml": "text/html",
		"/tmp/document.jpg": "image/jpeg",
		"/tmp/document.jpeg": "image/jpeg",
		"/tmp/document.jpe": "image/jpeg",
		"/tmp/document.jfif": "image/jpeg",
		"/tmp/document.jfif-tbnl": "image/jpeg",
		"/tmp/document.png": "image/png",
		"/tmp/document.tif": "image/tif",
		"/tmp/document.tiff": "image/tiff",
		"/tmp/document.ppm": "image/x-portable-pixmap",
		"/tmp/document.pnm": "image/x-portable-anymap",
		"/tmp/document.txt": "text/plain",
		"/tmp/other.iso": "application/octet-stream",
	}

	for p, ct := range pathContentType {
		dct := detectPathContentTypeByExt(p)
		if dct != ct {
			t.Errorf("Expected [%s] content type for [%s] path, but found [%s]", ct, p, dct)
		}
	}
}
