package convert

import (
	"errors"
	"io"
	"strings"

	"golang.org/x/net/context"
)

// DocTxt is the result of a converted document to text.
type DocTxt struct {
	// Pages extracted of the document
	Pages []*DocTxtPage `json:"pages,omitempty"`
}

// DocTxtPage is the result of a converted document page to text.
type DocTxtPage struct {
	// Text extracted of the page
	Text string `json:"text,omitempty"`
	// Additionnal informations on the extracted text
	Annotations map[string]interface{} `json:"annotations,omitempty"`
}

// Client is the client to use for conversions of any type of file.
// It should be closed when not longer use.
type Client struct {
	// The ImageClient to use for images conversions.
	ImageClient DocumentClient
	// The PdfClient to use for pdf conversions.
	PdfClient DocumentClient
}

// DocumentClient defines a client to use for document conversions.
type DocumentClient interface {
	// ConvertFromPath converts a document from the given path.
	ConvertFromPath(ctx context.Context, path string) (*DocTxt, error)
	// ConvertFromReader converts a document from the given Reader.
	ConvertFromReader(ctx context.Context, reader io.Reader) (*DocTxt, error)
	// Close closes the conversion client and its resources
	Close() error
}

// ConvertPath converts file from given path into text.
func (client *Client) ConvertPath(ctx context.Context, path string) (*DocTxt, error) {
	if len(path) == 0 {
		return nil, errors.New("[path] argument is required to convert a file from path")
	}

	contentType, err := detectPathContentType(path)
	if err != nil || contentType == fallbackContentType {
		contentType = detectPathContentTypeByExt(path)
		if contentType != fallbackContentType {
			err = nil
		}
	}
	if err != nil {
		return nil, err
	}
	if !IsSupportedContentType(contentType) {
		return nil, errors.New("Content type [" + contentType + "] of file [" + path + "] is not supported (yet ?)")
	}

	var doctxt *DocTxt

	if strings.HasPrefix(contentType, "image/") {
		if client.ImageClient == nil {
			return nil, errors.New("There is no ImageClient configured, the image [" + path + "] can not be converted")
		}
		doctxt, err = client.ImageClient.ConvertFromPath(ctx, path)
	} else if contentType == "application/pdf" {
		if client.PdfClient == nil {
			return nil, errors.New("There is no PdfClient configured, the pdf [" + path + "] can not be converted")
		}
		doctxt, err = client.PdfClient.ConvertFromPath(ctx, path)
	}

	if err != nil {
		return nil, err
	}
	return doctxt, nil
}

// Close closes the conversion client and its resources
func (client *Client) Close() error {
	var closeErrors []string

	if client.ImageClient != nil {
		err := client.ImageClient.Close()
		if err != nil {
			closeErrors = append(closeErrors, err.Error())
		}
	}

	if client.PdfClient != nil {
		err := client.PdfClient.Close()
		if err != nil {
			closeErrors = append(closeErrors, err.Error())
		}
	}

	if len(closeErrors) > 0 {
		return errors.New(strings.Join(closeErrors, "\n"))
	}
	return nil
}
