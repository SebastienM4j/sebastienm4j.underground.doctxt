package convert

// ClientBuilder helps to build a Client.
type ClientBuilder struct {
	imageBuilder *ImageClientBuilder
	pdfBuilder *PdfClientBuilder
}

// NewClientBuilder creates a new ClientBuilder.
func NewClientBuilder() *ClientBuilder {
	return &ClientBuilder{}
}

// ConvertImages configures Client for images conversions.
func (builder *ClientBuilder) ConvertImages() *ImageClientBuilder {
	builder.imageBuilder = NewImageClientBuilder(builder)
	return builder.imageBuilder
}

// ConvertPdf configures Client for pdf conversions.
func (builder *ClientBuilder) ConvertPdf() *PdfClientBuilder {
	builder.pdfBuilder = NewPdfClientBuilder(builder)
	return builder.pdfBuilder
}

// Build build the Client.
func (builder *ClientBuilder) Build() *Client {
	client := &Client{}
	if builder.imageBuilder != nil {
		client.ImageClient = builder.imageBuilder.Build()
	}
	if builder.pdfBuilder != nil {
		builder.pdfBuilder.defaultImageClient = client.ImageClient
		client.PdfClient = builder.pdfBuilder.Build()
	}
	return client
}

//
// ImageClientBuilder
//

// ImageClientBuilder helps to build an ImageClient.
type ImageClientBuilder struct {
	parent        *ClientBuilder
	visionBuilder *VisionImageClientBuilder
}

// NewImageClientBuilder creates a new ImageClientBuilder.
func NewImageClientBuilder(parent *ClientBuilder) *ImageClientBuilder {
	return &ImageClientBuilder{parent: parent}
}

// WithVision configures a VisionImageClientBuilder
// for images conversions done with the Google Vision API.
func (builder *ImageClientBuilder) WithVision() *VisionImageClientBuilder {
	builder.visionBuilder = NewVisionImageClientBuilder(builder)
	return builder.visionBuilder
}

// And permits to continue the Client configuration.
func (builder *ImageClientBuilder) And() *ClientBuilder {
	return builder.parent
}

// Build build the ImageClient.
func (builder *ImageClientBuilder) Build() DocumentClient {
	if builder.visionBuilder != nil {
		return builder.visionBuilder.Build()
	}
	return nil
}

//
// PdfClientBuilder
//

// PdfClientBuilder helps to build an PdfClient.
type PdfClientBuilder struct {
	parent             *ClientBuilder
	defaultImageClient DocumentClient
	imageClient        DocumentClient
	popplerBuilder     *PopplerPdfClientBuilder
}

// NewPdfClientBuilder creates a new ImageClientBuilder.
func NewPdfClientBuilder(parent *ClientBuilder) *PdfClientBuilder {
	return &PdfClientBuilder{parent: parent}
}

// ImageClient defines the ImageClient to use for images conversions in pdf.
func (builder *PdfClientBuilder) ImageClient(imageClient DocumentClient) *PdfClientBuilder {
	builder.imageClient = imageClient
	return builder
}

// WithPoppler configures a PopplerPdfClientBuilder
// for pdf conversions done with poppler-utils library.
func (builder *PdfClientBuilder) WithPoppler() *PopplerPdfClientBuilder {
	builder.popplerBuilder = NewPopplerPdfClientBuilder(builder)
	return builder.popplerBuilder
}

// And permits to continue the Client configuration.
func (builder *PdfClientBuilder) And() *ClientBuilder {
	return builder.parent
}

// Build build the ImageClient.
func (builder *PdfClientBuilder) Build() DocumentClient {
	if builder.popplerBuilder != nil {
		popplerPdfClient := builder.popplerBuilder.Build()
		if builder.imageClient != nil {
			popplerPdfClient.imageClient = builder.imageClient
		} else {
			popplerPdfClient.imageClient = builder.defaultImageClient
		}
		return popplerPdfClient
	}
	return nil
}
