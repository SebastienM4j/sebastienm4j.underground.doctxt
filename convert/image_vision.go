package convert

import (
	"encoding/json"
	"errors"
	"io"
	"os"

	"golang.org/x/net/context"
	"google.golang.org/api/option"
	pb "google.golang.org/genproto/googleapis/cloud/vision/v1"
	// vision "cloud.google.com/go/vision/apiv1"
)

// VisionImageClient is an ImageClient that use Google Vision API to make OCR.
type VisionImageClient struct {
	// The Google Vision Service Account file path for OCR
	// If it's not provided, environnement variable `GOOGLE_APPLICATION_CREDENTIALS`
	// should be set (see https://developers.google.com/identity/protocols/application-default-credentials)
	serviceAccountFile string
	// Client of the Google Vision API
	apiClient ImageAnnotatorClient
}

// ConvertFromPath converts an image from the given path with Google Vision API.
func (client *VisionImageClient) ConvertFromPath(ctx context.Context, path string) (*DocTxt, error) {
	if len(path) == 0 {
		return nil, errors.New("[path] argument is required to convert an image from path")
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	doctxt, err := client.ConvertFromReader(ctx, file)
	if err != nil {
		return nil, err
	}
	return doctxt, nil
}

// ConvertFromReader converts an image from the given Reader with Google Vision API.
func (client *VisionImageClient) ConvertFromReader(ctx context.Context, reader io.Reader) (*DocTxt, error) {
	apiClient, err := client.getAPIClient(ctx)
	if err != nil {
		return nil, err
	}

	image, err := vision.NewImageFromReader(reader)
	if err != nil {
		return nil, err
	}

	features := []*pb.Feature{
		{Type: pb.Feature_TEXT_DETECTION},
		{Type: pb.Feature_DOCUMENT_TEXT_DETECTION},
	}

	request := &pb.AnnotateImageRequest{
		Image:        image,
		ImageContext: nil,
		Features:     features,
	}

	response, err := apiClient.AnnotateImage(ctx, request)
	if err != nil {
		return nil, err
	}
	if response.Error != nil {
		jsonError, _ := json.Marshal(response.Error)
		return nil, errors.New("Google Vision API response contains errors [" + string(jsonError) + "]")
	}

	page := &DocTxtPage{}
	page.Annotations = make(map[string]interface{})

	if response.FullTextAnnotation != nil {
		page.Text = response.FullTextAnnotation.Text
		if len(response.FullTextAnnotation.Pages) > 0 {
			page.Annotations["vision_page"] = response.FullTextAnnotation.Pages
		}
	}

	if response.TextAnnotations != nil && len(response.TextAnnotations) > 0 {
		if len(page.Text) == 0 {
			page.Text = response.TextAnnotations[0].Description
		}
		page.Annotations["vision_text"] = response.TextAnnotations
	}

	doctxt := &DocTxt{
		Pages: []*DocTxtPage{page},
	}
	return doctxt, nil
}

// getAPIClient return the existing vision.ImageAnnotatorClient or create a new one.
func (client *VisionImageClient) getAPIClient(ctx context.Context) (ImageAnnotatorClient, error) {
	if client.apiClient != nil {
		return client.apiClient, nil
	}

	var apiClient ImageAnnotatorClient
	var err error
	if len(client.serviceAccountFile) == 0 {
		apiClient, err = vision.NewImageAnnotatorClient(ctx)
	} else {
		opt := option.WithServiceAccountFile(client.serviceAccountFile)
		apiClient, err = vision.NewImageAnnotatorClient(ctx, opt)
	}
	if err != nil {
		return nil, err
	}
	client.apiClient = apiClient

	return client.apiClient, nil
}

// Close closes this VisionImageClient and its resources
func (client *VisionImageClient) Close() error {
	if client.apiClient != nil {
		return client.apiClient.Close()
	}
	return nil
}

//
// Builder
//

// VisionImageClientBuilder helps to build a VisionImageClient.
type VisionImageClientBuilder struct {
	parent             *ImageClientBuilder
	serviceAccountFile string
}

// NewVisionImageClientBuilder creates a new VisionImageClientBuilder.
func NewVisionImageClientBuilder(parent *ImageClientBuilder) *VisionImageClientBuilder {
	return &VisionImageClientBuilder{parent: parent}
}

// AuthWithServiceAccount configures the path of the service acount file for authentication.
//
// If it's not provided, environnement variable `GOOGLE_APPLICATION_CREDENTIALS`
// should be set (see https://developers.google.com/identity/protocols/application-default-credentials).
func (builder *VisionImageClientBuilder) AuthWithServiceAccount(serviceAccountFile string) *VisionImageClientBuilder {
	builder.serviceAccountFile = serviceAccountFile
	return builder
}

// And permits to continue the Client configuration.
func (builder *VisionImageClientBuilder) And() *ClientBuilder {
	return builder.parent.parent
}

// Build build the VisionImageClient.
func (builder *VisionImageClientBuilder) Build() *VisionImageClient {
	return &VisionImageClient{serviceAccountFile: builder.serviceAccountFile}
}
