DocTxt Change Log
=================

1.0.0 / _2018-03-06_
--------------------

* Command Line Interface (CLI) to convert images or PDF from a path
* Conversion of images with [Google Vision API](https://cloud.google.com/vision/docs/)
* Conversion of PDF with [poppler-utils](https://launchpad.net/ubuntu/+source/poppler)
