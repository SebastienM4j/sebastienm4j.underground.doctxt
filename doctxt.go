package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"io/ioutil"

	"golang.org/x/net/context"

	"gitlab.com/SebastienM4j/doctxt/convert"
)

var (
	inputPath string
	outputPath string
	visionServiceAccountFile string
)

func flagStringVar(value *string, shortFlag string, longFlag string, defaultValue string, usage string) {
	if len(shortFlag) > 0 {
		shortUsage := usage
		if len(longFlag) > 0 {
			shortUsage = "Alias for '-"+longFlag+"'"
		}
		flag.StringVar(value, shortFlag, defaultValue, shortUsage)
	}
	if len(longFlag) > 0 {
		flag.StringVar(value, longFlag, defaultValue, usage)
	}
}

func initFlags() {
	flagStringVar(&inputPath, "i", "input", inputPath, "Path to the document to convert\n\tOptional if 'document' argument is provided")
	flagStringVar(&outputPath, "o", "output", outputPath, "Path to the converted document\n\tOptional, instead the converted document is printed on the standard output")
	flagStringVar(&visionServiceAccountFile, "vak", "vision.auth.key", visionServiceAccountFile, "Path to the service account file for Google Vision API authentication\n\tOptional if 'GOOGLE_APPLICATION_CREDENTIALS' environnement variable is set\n\t(see https://developers.google.com/identity/protocols/application-default-credentials)")

	flag.Usage = func() {
		fmt.Println("Usage of doctxt :")
		fmt.Println("")
		fmt.Printf("  %s [options]... [document]\n", os.Args[0])
		fmt.Println("")
		fmt.Println("options : ")
		flag.PrintDefaults()
		fmt.Println("")
		fmt.Println("document :")
		fmt.Println("  Path to the document to convert")
		fmt.Println("  Optional if '-i' or '-input' option is provided")
	}

	flag.Parse()

	if inputPath == "" {
		inputPath = flag.Arg(0)
	}
}

func main() {
	// Flags and Args

	initFlags()

	if inputPath == "" {
		flag.Usage()
		os.Exit(-1)
	}

	// Build client

	clientBuilder := convert.NewClientBuilder()
	clientBuilder.ConvertImages().WithVision().AuthWithServiceAccount(visionServiceAccountFile)
	clientBuilder.ConvertPdf().WithPoppler()

	client := clientBuilder.Build()
	defer client.Close()

	// Convert and marshal

	doctxt, err := client.ConvertPath(context.Background(), inputPath)
	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}

	out, err := json.Marshal(doctxt)
	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}

	// Output result

	if outputPath == "" {
		_, err = os.Stdout.Write(out)
	} else {
		err = ioutil.WriteFile(outputPath, out, 0664)
	}
	if err != nil {
		log.Fatal(err)
		os.Exit(-1)
	}
}
